Repositório para criar API de matrículas, cujo deve conter os dados:
-Aluno: CPF, nome, data de nascimento, RG, órgão emissor.
-Curso: Nome, data de criação, coordenador, nome do prédio.
-Disciplinas: Nome, código, breve descrição.
-Professor: CPF, nome, titulação.

Realizar as associações necessárias entre os dados.

Utilizar o software SQLite3 e a linguagem Python.